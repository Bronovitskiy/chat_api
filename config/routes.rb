Rails.application.routes.draw do
  resources :listings
  mount_devise_token_auth_for 'User', at: 'auth'
end
